#!/bin/zsh
# Setup script for setting up a new macos machine
echo "Setting up your new MacOS machine..."
# Check for xcode CLI to be present as a prerequisite for Homebrew, install if it's missing
which xcode-select
if [[ $? != 0 ]] ; then
    echo "Installing xcode CLI because it's not installed.."
    xcode-select --install
else
    echo "xcode CLI is already installed.."
fi
# Check for Homebrew to be present, install if it's missing
which brew
if [[ $? != 0 ]] ; then
    echo "Installing Homebrew because it's not installed.."
    ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
else
    echo "Updating Homebrew because it was already installed.."
    brew update
fi
# Disable homebrew analytics
brew analytics off
# Install homebrew packages
PACKAGES=(
    youtube-dl
    wget
    whois
    mas
    speedtest-cli
)
echo "Installing packages..."
brew install ${PACKAGES[@]}
# Install homebrew casks
CASKS=(
    authy
    bitwarden
    brave-browser
    cryptomator
    discord
    firefox
    flux
    freetube
    gyazo
    handbrake
    libreoffice
    licecap
    megasync
    netnewswire
    notion
    obs
    openshot-video-editor
    responsively
    signal
    standard-notes
    telegram
    tor-browser
    visual-studio-code
    vlc
    whatsapp
    zoom
)
echo "Installing cask apps..."
brew install --cask ${CASKS[@]}
# Update all Brew formulae and casks
echo "Updating all formulae and casks installed by Brew.."
brew upgrade --greedy 
# Clean brew cache
echo "Cleaning up brew cache..."
brew cleanup
# Update OS and any OS installed Apps
echo "Updating OS and any OS installed Apps.."
softwareupdate --install --all
mas upgrade
# Update NPM Packages Globally 
#echo "Updating globally installed NPM packages.."
#npm update -g
# MacOS Configuration Commands = https://macos-defaults.com/
echo "Configuring OS..."
# Dock 
echo "Applying custom settings to the Dock.."
# Autohide Dock & kill the dock so the settings are applied
defaults write com.apple.Dock autohide-delay -float 0 && killall Dock
# Set animation time for Dock to 1 second & kill the dock so the settings are applied
defaults write com.apple.dock autohide-time-modifier -int 1 && killall Dock
# Put the Dock on the bottom of the screen
defaults write com.apple.dock "orientation" -string "bottom" && killall Dock
# Set dock icon size
defaults write com.apple.dock "tilesize" -int "41" && killall Dock
# Hide recently opened  apps in the dock
defaults write com.apple.dock "show-recents" -bool "false" && killall Dock
# Pin specific apps to Dock
# clear items
defaults write com.apple.dock persistent-apps -array && killall Dock
echo "Dock has been cleared of any persistent apps"
# Doesn't work without this sleep command
sleep 5
# Add specific apps to dock
dockItems=(
  "/System/Applications/Stocks.app"
  "/System/Applications/Preview.app"
  "/System/Applications/Utilities/Terminal.app"
  "/Applications/FreeTube.app"
  "/Applications/NetNewsWire.app"
  "/Applications/WhatsApp.app"
  "/Applications/Telegram.app"
  "/Applications/Notion.app"
  "/Applications/Brave Browser.app"
  "/Applications/Standard Notes.app"
  "/Applications/Visual Studio Code.app"
  "/Applications/Safari.app"
  "/Applications/LibreOffice.app"
)
# Loop through the dock items and add them to persistent-apps using defaults write
for dockItem in "${dockItems[@]}"; do
  defaults write com.apple.dock persistent-apps -array-add \
    "<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>${dockItem}</string><key>_CFURLStringType</key><integer>0</integer></dict></dict></dict>"
done
echo "Added persistent apps to Dock"
# Restart the Dock for changes to take effect
killall Dock
echo 'Finished applying custom settings to the Dock!'
# Menu bar = show/hide ???
echo 'Applying custom settings to the Menu Bar..'
# Set date/time format
defaults write com.apple.menuextra.clock "DateFormat" -string "\"EEE d MMM HH:mm:ss\"" 
# Set timezone
sudo systemsetup -settimezone Asia/Nicosia
# Disable crash reporting & Analytics
echo "Disabling crash reporting & analytics.."
# To disable sending diagnostics and usage data to Apple
defaults write com.apple.SubmitDiagInfo AutoSubmit -bool false
# To disable sending crash data to Apple
defaults write com.apple.CrashReporter DialogType none
# To disable sending crash data to app developers
defaults write com.apple.CrashReporter UseUNC 0
# Enable firewall & stealth mode
echo "Enabling firewall & stealth mode.."
# Turn firewall on
sudo defaults write /Library/Preferences/com.apple.alf globalstate -int 1
# Enable stealth mode for firewall so it doesn't respond or acknowledge connection attempts
sudo defaults write /Library/Preferences/com.apple.alf stealthenabled -int 1
echo 'Finished applying custom settings to the Menu Bar!'
#Install oh-my-zsh framework for zsh shell 
echo "Installing oh-my-zsh framework for zsh shell.."
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
echo "oh-my-zsh is installed"
# Screensaver 
# Start screen saver after 10 minutes (Increase it for movies?)
defaults -currentHost write com.apple.screensaver idleTime -int 600
# Require password immediately after sleep or screen saver begins
defaults write com.apple.screensaver askForPassword -bool true
defaults write com.apple.screensaver askForPasswordDelay -int 0
# What other OS settings?
# Aliases
# Trackpad
# Bash Profile
# Cronjobs - autoupdate everything every Sunday
# import any dotfiles, scripts or other configuration files from old machine, github or google drive?
# configure zsh shell
# Misc - set desktop wallpaper, set default applications, setting language & region, disable location services, save which apps should open on login, enable filevault. 
echo "Macbook setup completed!"
echo "Restart the computer manually to apply all changes!"



